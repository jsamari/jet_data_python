# imports
import ROOT
import numpy as np

def JetConstData(fileName, JetContName):
    # Initialize the xAOD infrastructure
    ROOT.xAOD.Init()

    # Set up the input file
    treeName = 'CollectionTree'
    f = ROOT.TFile.Open(fileName)

    # Make the "transient tree"
    t = ROOT.xAOD.MakeTransientTree(f, treeName)

    #initialize empty array with shape of 1 constit's data
    constituentData = np.empty((1,7))

    #loop through events, jets, then constituents
    i = 0
    for entry in np.arange(t.GetEntries()):
        t.GetEntry(entry)

        #check the event does not have jets
        if getattr(t, JetContName).size()!= 0:

            #jet loop
            for jet in getattr(t, JetContName):  
                numConstits = jet.numConstituents()
                #constituent loop
                for k in range(numConstits):
                    constit = jet.rawConstituent(k)
                    if constit.pt() != float(0) or constit.eta() != float(0) or constit.e() != float(0) or constit.phi() != float(0):
                        constituentData = np.append(constituentData, [[constit.eta(), constit.phi(),constit.pt(),constit.e(),constit.charge(), constit.m(), constit.rapidity()]], axis=0)
            i=i+1
    constituentData = np.delete(constituentData, 0, 0)

    return constituentData